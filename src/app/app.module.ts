import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {Router} from '@angular/router';


import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ApolloModule, Apollo} from 'apollo-angular';
import {HttpLinkModule, HttpLink} from 'apollo-angular-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {environment} from '../environments/environment';
import {onError} from 'apollo-link-error';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { DialogModule } from 'primeng/dialog';


/* Components */
import {HomeComponent} from './home/home.component';
import {MainMenuComponent} from './main-menu/main-menu.component';

/* Services */
import {DefaultService} from '../services/default.service';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        MainMenuComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        ApolloModule,
        HttpLinkModule,
        HttpClientModule,
        FormsModule,
        DialogModule,

    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(private httpLink: HttpLink,
                private apollo: Apollo,
                private router: Router
    ) {
        const apolloHttpLink = httpLink.create({
            uri: `${environment.merlin}/graphql`
        });
        const apolloErrorLink = onError(({graphQLErrors, networkError}) => {
            if (graphQLErrors) {
                graphQLErrors.map(({message, locations, path}) =>
                    this.handleGraphError(message),
                );
            }

            if (networkError) {
                this.handleNetworkError(networkError);
            }
        });
        apollo.create({
            link: apolloErrorLink.concat(apolloHttpLink),
            cache: new InMemoryCache()
        });
    }

    handleGraphError(graphQLError) {
        console.log('Graph Error: ', graphQLError);

        if (graphQLError === 'Unauthorized') {
            this.router.navigate(['/']);
        }

    }

    handleNetworkError(networkError) {
        let error: any = {};

        if (networkError.error && typeof networkError.error === 'string') {
            error = JSON.parse(networkError.error);
        }

        switch (networkError.status) {
            case 0:
            case 400:
            case 500: {
                //

                break;
            }
            case 401: {
                //
                break;
            }
            case 403: {
                //
                break;
            }
            case 404: {
                //

                break;
            }
        }
    }
}
