import {Component, OnInit} from '@angular/core';
import {DefaultService} from '../../services/default.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
    currentCash: any;
    newBaseList;
    moneyList: [any];
    newPaymentCash;
    paymentModal: boolean;
    baseModal: boolean;
    total: number;
    cost: number;
    paymentName: string;
    relay;

    constructor(private defaultService: DefaultService) {
        this.currentCash = [];
        this.total = 0;
        this.paymentModal = false;
        this.paymentName = '';
        this.cost = 0;
        this.newBaseList = [];
        this.newPaymentCash = [];
    }


    ngOnInit(): void {
        this.getCurrentCash();
        this.getMoneys();
    }

    emptyBox(): void {
        this.defaultService.emptyBox().subscribe(({data}) => {
            alert(data.emptyBoxMutation.message);
            this.defaultService.invalidateCache().then(result => {
                this.getCurrentCash();
            });
        });
    }

    cashRegisterBase(): void {
        this.defaultService.newBase(this.newBaseList).subscribe(({data}) => {
            if (data.cashRegisterBaseMutation.success) {
                this.baseModal = false;
                this.newBaseList = [];
                alert(data.cashRegisterBaseMutation.message);
                this.defaultService.invalidateCache().then(result => {
                    this.getCurrentCash();
                });
            } else {
                alert(data.cashRegisterBaseMutation.message);
            }
        });
    }

    getCurrentCash(): void {
        this.defaultService.currentCashStatus().subscribe(({data}) => {
            this.currentCash = data.currentCashStatusQuery;
        });
    }

    getMoneys(): void {
        this.defaultService.getMoneys().subscribe(({data}) => {
            this.moneyList = data.moneysQuery;
            console.log(this.moneyList);
        });
    }

    newPaymentModal(): void {
        this.paymentModal = true;
    }

    newBaseModal(): void {
        this.baseModal = true;
    }

    changeValue(money, units: number): void {
        this.total = 0;
        const moneyPayment = this.newPaymentCash.find((moneyOld: any) => moneyOld.money_id === money.money_id);
        if (moneyPayment) {
            moneyPayment.units = Number(units);
        } else {
            if (units > 0) {
                this.newPaymentCash.push({money_id: money.money_id, units: Number(units), value: money.value});
            }
        }
        this.newPaymentCash.forEach(element => {
            if (element) {
                console.log(element.value * element.units);
                this.total = this.total + (element.value * element.units);
            }
        });
    }

    changeValueBase(money, units: number): void {
        const moneyPayment = this.newBaseList.find((moneyOld: any) => moneyOld.money_id === money.money_id);
        if (moneyPayment) {
            moneyPayment.units = Number(units);
        } else {
            if (units > 0) {
                this.newBaseList.push({money_id: money.money_id, units: Number(units)});
            }
        }
        console.log('base', this.newBaseList);
    }

    registerPayment(): void {
        const newCash = [];
        this.newPaymentCash.forEach(paymentCash => {
            newCash.push({money_id: paymentCash.money_id, units: paymentCash.units});
        });
        const payment = {
            name: this.paymentName,
            value: this.cost,
            cash: newCash
        };
        this.defaultService.newPayment(payment).subscribe(({data}) => {
            if (data.newPaymentMutation.success) {
                console.log(data.newPaymentMutation);
                if (data.newPaymentMutation.relay.cash) {
                    this.relay = data.newPaymentMutation.relay.cash;
                    console.log('entro', this.relay);
                }
                alert(data.newPaymentMutation.message);

                this.paymentModal = false;
                this.newPaymentCash = [];
                this.paymentName = '';
                this.total = 0;
                this.cost = 0;
                this.defaultService.invalidateCache().then(result => {
                    this.getCurrentCash();
                });
            } else {
                alert(data.newPaymentMutation.message);
            }
        });
    }

}
