import gql from 'graphql-tag';

export const CurrentCashStatusQuery = gql`
    query {
       currentCashStatusQuery{
           cash{
              units
              money{
                money_id
                value
              }
            }
            total_cash
          }
    }
`;
export const MoneysQuery = gql`
    query {
        moneysQuery{
            money_id
            value
        }
    }
`;
