import gql from 'graphql-tag';

export const CashRegisterBaseMutation = gql`
    mutation($cash_base:[MoneyInput!]) {
       cashRegisterBaseMutation(cash_base:$cash_base){
          success
          message
       }
    }
`;
export const EmptyBox = gql`
    mutation {
       emptyBoxMutation{
          success
          message
       }
    }
`;

export const NewPaymentMutation = gql`
    mutation($payment:PaymentInput!) {
       newPaymentMutation(payment:$payment){
          success
          message
          relay{
             cash{
                money{
                  value
                }
                units
             }
          total_cash
        }
       }
    }
`;
