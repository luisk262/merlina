import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {CurrentCashStatusQuery, MoneysQuery} from '../graphql/querys.graph';
import {CashRegisterBaseMutation, EmptyBox, NewPaymentMutation} from '../graphql/mutations.graph';


@Injectable({
    providedIn: 'root'
})
export class DefaultService {

    constructor(private apollo: Apollo,
                private http: HttpClient) {
    }

    invalidateCache() {
        return this.apollo.getClient().resetStore();
    }

    getMoneys(): Observable<any> {
        return this.apollo.query<any>({
            query: MoneysQuery
        });
    }

    currentCashStatus(): Observable<any> {
        return this.apollo.query<any>({
            query: CurrentCashStatusQuery
        });
    }

    newBase(moneyList): Observable<any> {
        return this.apollo.mutate<any>({
            mutation: CashRegisterBaseMutation,
            variables: {
                cash_base: moneyList
            }
        });
    }

    emptyBox(): Observable<any> {
        return this.apollo.mutate<any>({
            mutation: EmptyBox
        });
    }

    newPayment(payment): Observable<any> {
        return this.apollo.mutate<any>({
            mutation: NewPaymentMutation,
            variables: {payment}
        });
    }
}
